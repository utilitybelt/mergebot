import commandLineArgs from 'command-line-args';
import makeReleaseCandidate from './commands/make-release-candidate.js'
import mergeReleaseMaster from './commands/merge-release-master.js'


let mainDefinitions = [
  { name: 'name', defaultOption: true }
]
const mainCommand = commandLineArgs(mainDefinitions, { stopAtFirstUnknown: true })
let argv = mainCommand._unknown || []

if (mainCommand.name === 'make-release-candidate') {
  const rcDefinitions = [
    { name: 'project', alias: 'p', type: String, defaultOption: true },
    { name: 'id', type: Number },
    { name: 'sha', alias: 's', type: String },
  ]

  makeReleaseCandidate(commandLineArgs(rcDefinitions, { argv }))
}
else if (mainCommand.name === 'merge-release-master') {
  const rcDefinitions = [
    { name: 'project', alias: 'p', type: String, defaultOption: true },
    { name: 'name', alias: 'n', type: String },
    { name: 'id', type: Number },
    { name: 'sha', alias: 's', type: String },
    { name: 'version', alias: 'v', type: String },
  ]

  mergeReleaseMaster(commandLineArgs(rcDefinitions, { argv }))
}
else {
  console.log("Unknown command");
}