import { simpleGit } from 'simple-git';
import { promisify } from 'util'
import { exec } from 'child_process'
import { Gitlab } from '@gitbeaker/node';
import fs from 'fs'
import fetch from 'node-fetch';

const gitlab = new Gitlab({
  host: 'https://gitlab.com',
  token: process.env.MERGEBOT_TOKEN,
});

const execAsync = promisify(exec);

const tempRepoDir = './tmp'
const groupId = 6617010

export default async function mergeReleaseMaster(options) {
  const projectSlug = options['project']
  const projectName = options['name']
  const projectId = options['id']
  const baseCommit = options['sha']
  const version = options['version']

  try {
    const changelogNextPath = 'changelog/vNext.md'
    await cloneRepo(projectSlug, tempRepoDir)
    await simpleGit().merge(['origin/release'])
    const changelog = fs.readFileSync(`changelog/v${version}.md`).toString()
    await simpleGit().addAnnotatedTag(`v${version}`, changelog)
    await simpleGit().pushTags()
    await simpleGit().commit(`Merge release v${version}`)
    await run(`echo "- Changes go here" > ${changelogNextPath}`)
    await simpleGit().add(changelogNextPath)
    await simpleGit().commit(`Bump version`)
    await simpleGit().push('origin', 'master')

    const { packageName, links } = await getPackageLinks(projectId, projectSlug, version)

    gitlab.Releases.create(projectId, {
      name: `${projectName} v${version}`,
      tag_name: `v${version}`,
      description: changelog,
      assets: {
        links: links
      }
    })
    
    if (projectId == 41351198) {
      var fields = [
        {
            "name": "Changes",
            "value": changelog,
            "inline": false
        }
      ];
    
      links.forEach(link => {
        if (link.name.endsWith(".exe")) {
          fields.push({
            "name": "Download",
            "value": `[${link.name}](${link.url})`,
            "inline": false
          });
        }
      })
    
      var params = {
        content: `Version ${version} of UtilityBelt.Service has been released.`,
        embeds: [
            {
                "title": `v${version}`,
                "fields": fields,
            }
        ]
      };
      fetch(process.env.DISCORD_HOOK_URL, {
        method: "POST",
        headers: {
            'Content-type': 'application/json'
        },
        body: JSON.stringify(params)
      })

    }
    
  }
  catch (ex) {
    console.error(ex)
    process.exit(1)
  }
}

async function getPackageLinks(projectId, projectName, version) {
  console.log('Version: ', version);
  const packages = await gitlab.Packages.all({ projectId })
  let pkg = undefined;

  const links = []

  let foundNuget = false;
  let foundPackage = false;
  let foundInstaller = false;

  for (var i = 0; i < packages.length; i++) {
    if (foundPackage && foundNuget && foundInstaller) {
      break;
    }

    if (packages[i].version === version) {
      pkg = packages[i]
      const pkgFiles = await gitlab.Packages.showFiles(projectId, pkg.id)
      let ext = 'zip'
      let name = `${pkg.name}.${pkg.version}.${ext}`;


      if (!foundNuget && pkg.package_type == 'nuget') {
        ext = 'nupkg'
        foundNuget = true;
        name = `${pkg.name}.${pkg.version}.${ext}`;
      }
      else if (!foundPackage && pkg.package_type == 'generic' && pkgFiles[0].file_name.endsWith(".zip")) {
        ext = 'zip'
        foundPackage = true;
        name = `${pkg.name}.${pkg.version}.${ext}`;
      }
      else if (!foundInstaller && pkg.package_type == 'generic' && pkgFiles[0].file_name.endsWith(".exe")) {
        ext = 'exe'
        foundInstaller = true;
        name = `${pkg.name}-Installer-${pkg.version}.${ext}`;
      }
      else {
        continue;
      }

      links.push({
        name: `${name}`,
        filepath: `/packages/${name}`,
        url: `https://gitlab.com/${projectName}/-/package_files/${pkgFiles[0].id}/download`,
        link_type: 'package'
      })
    }
  }

  return { packageName: projectName, links: links }
}

async function cloneRepo(project, tempRepoDir) {
  console.log(`Cloning ${project} to ${tempRepoDir}`);
  const remote = `https://${process.env.MERGEBOT_USER}:${process.env.MERGEBOT_PASS}@gitlab.com/${project}.git`;

  await execAsync(`rm -rf ${tempRepoDir}`);
  await simpleGit().clone(remote, tempRepoDir)

  process.chdir(tempRepoDir)

  await simpleGit().addConfig('user.email', process.env.CI_GIT_USER_EMAIL, false, 'local')
  await simpleGit().addConfig('user.name', process.env.CI_GIT_USER_USERNAME, false, 'local')
}

async function run(command) {
  const { stdout, stderr } = await execAsync(command)
  if (stderr) {
      throw(`stderr: ${stderr}`)
  }

  return stdout
}