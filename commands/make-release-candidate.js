import { simpleGit } from 'simple-git';
import { promisify } from 'util'
import { exec } from 'child_process'
import { Gitlab } from '@gitbeaker/node';
import fs from 'fs'


const gitlab = new Gitlab({
  host: 'https://gitlab.com',
  token: process.env.MERGEBOT_TOKEN,
});

const execAsync = promisify(exec);

const tempRepoDir = './tmp'

export default async function makeReleaseCandidate(options) {
  const projectName = options['project']
  const projectId = options['id']
  const baseCommit = options['sha']

  console.log(`makeReleaseCandidate: name: ${projectName} id: ${projectId} baseCommit: ${baseCommit}`);

  try {
    await cloneRepo(projectName, tempRepoDir)
    const gitversion = await getGitVersion()
    const changelogNextPath = 'changelog/vNext.md'
    const changelogVersionedPath = `changelog/v${gitversion['MajorMinorPatch']}.md`
    const releaseCandidateBranch = `releases/v${gitversion.MajorMinorPatch}`

    console.log(`Checking out branch ${releaseCandidateBranch}`)
    const git = await simpleGit().checkoutBranch(releaseCandidateBranch, baseCommit)
    console.log(`mv "${changelogNextPath}" "${changelogVersionedPath}"`)
    await run(`mv "${changelogNextPath}" "${changelogVersionedPath}"`)
    console.log(`git rm ${changelogNextPath}`)
    await simpleGit().rm(changelogNextPath)
    console.log(`git add ${changelogVersionedPath}`)
    await simpleGit().add(changelogVersionedPath)
    console.log(`git commit v${gitversion.MajorMinorPatch} release candidate`)
    await simpleGit().commit(`v${gitversion.MajorMinorPatch} release candidate`, [changelogNextPath, changelogVersionedPath])
    console.log(`git push origin ${releaseCandidateBranch}`)
    await simpleGit().push('origin', releaseCandidateBranch)

    gitlab.MergeRequests.create(projectId, releaseCandidateBranch, 'release', `Release v${gitversion.MajorMinorPatch}`, {
      removeSourceBranch: true,
      labels: ['release-candidate'],
      squash: true,
      description: fs.readFileSync(changelogVersionedPath).toString(),
    })
  }
  catch (ex) {
    console.error(ex)
    process.exit(1)
  }
}

async function cloneRepo(project, tempRepoDir) {
  console.log(`Cloning ${project} to ${tempRepoDir}`);
  const remote = `https://${process.env.MERGEBOT_USER}:${process.env.MERGEBOT_PASS}@gitlab.com/${project}.git`;

  await execAsync(`rm -rf ${tempRepoDir}`);
  await simpleGit().clone(remote, tempRepoDir)

  process.chdir(tempRepoDir)

  await simpleGit().addConfig('user.email', process.env.CI_GIT_USER_EMAIL, false, 'local')
  await simpleGit().addConfig('user.name', process.env.CI_GIT_USER_USERNAME, false, 'local')
}

async function getGitVersion() {
  const stdout = await run('dotnet-gitversion')
  return JSON.parse(stdout)
}

async function run(command) {
  const { stdout, stderr } = await execAsync(command)
  if (stderr) {
      throw(`stderr: ${stderr}`)
  }

  return stdout
}